# Lógica de Hoare para Progrmación Funcional

Universidad Nacional Autónoma de México, Facultad de Ciencias.

Proyecto de Tesis para obtener el Título de Licenciado en Ciencias de la Computación.

Presenta Gilberto Isaac López García.

Bajo dirección de la Dra. Lourdes del Carmen González Huesca

## Estructura del repositorio

El directorio `NFuncional` contiene la implementación del lenguaje funcional simple introducido en el segundo capítulo del trabajo.

El directorio `PCF` contiene la implementación del lenguaje PCF empleando la representación local anónima introducito en el tercer capítulo del trabajo.

## Requerimientos

Para utilizar el software propocionado en este repositorio es necesario contar con el asistente de pruebas [Coq](https://coq.inria.fr/), versión 8.12.

Para compilar la implementación del lenguaje PCF es necesario contar con la biblioteca [`TLC`](http://www.chargueraud.org/softs/tlc/) instalada. La biblioteca se puede descargar y compilar del repositorio de GitHub (para la versión 8.12 de Coq).

## Compilación

Dentro del directorio `NFuncional` se puede compilar el código usando el comando

```bash
    make
```

Se pueden limpiar los archivos generados por la compilación usando el comando

```bash
    make clean
```

Para compilar el ejemplo de verificación de la función factorial usar el siguiente comando

```bash
    coqc Factorial.v
```

Dentro del directorio `PCF` se puede compilar el código usando el comando

```bash
    make
```

Se pueden limpiar los archivos generados por la compilación usando el comando

```bash
    make clean
```


## Licencia

El código se distribuye bajo la licencia MIT. Ver arhivo LICENSE.
