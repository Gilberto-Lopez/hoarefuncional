Require Import Utf8.
Require Import Ids Maps.

Set Implicit Arguments.

Declare Scope pmap_notation_scope.

(* Funciones parciales o Partial Maps (PMaps).
 * Funciones parciales ∀ X, id → option X. *)
Definition pmap (X : Type) := map (option X).

(* La función parcial que no está definida en ningún punto (null) es la función
 * constante None. *)
Definition pmap_null {X : Type} : pmap X :=
  map_const None.

Notation "{ --> }" := pmap_null (at level 0)
                      : pmap_notation_scope.

(* Actualización de una función parcial en un punto. *)
Definition pmap_update {X : Type} (m : pmap X) (x : id) (v : X) : pmap X :=
  map_update m x (Some v).

Notation "m '&' { x --> v }" := (pmap_update m x v) (at level 20)
                                : pmap_notation_scope.

Open Scope pmap_notation_scope.

(* La función parcial null siempre regresa None. *)
Lemma pmap_apply_const : ∀ (X : Type) x,
    @pmap_null X x = None.
Proof.
  intros X x.
  apply map_apply_const.
Qed.

(* Actualizar una función parical en x y aplicarla en x resulta en el nuevo
 * valor. *)
Lemma pmap_update_eq : ∀X (m : pmap X) x v,
    m & { x --> v } x = Some v.
Proof.
  intros X m x v.
  apply map_update_eq.
Qed.

(* El valor de la función parcial actualizada en un punto en otros puntos
 * distintos no cambia. *)
Lemma pmap_update_neq : ∀X (m : pmap X) x y v,
    x ≠ y →
    m & { x --> v } y = m y.
Proof.
  intros X m x y v.
  apply map_update_neq.
Qed.

(* Actualizar el valor en un punto oculta actualizaciones previas. *)
Lemma pmap_update_shadow : ∀X (m : pmap X) x v w,
    m & { x --> v } & { x --> w }  = m & { x --> w }.
Proof.
  intros X m x v w.
  apply map_update_shadow.
Qed.

(* Actualizar el valor de la función parcial por su valor no afecta. *)
Lemma pmap_update_same : ∀X (m : pmap X) x v,
    m x = Some v →
    m & { x --> v } = m.
Proof.
  intros X m x v H.
  unfold pmap_update.
  rewrite <- H.
  apply map_update_same.
Qed.

(* El orden en que se actualicen puntos distintos de la función parcial no
 * afecta. *)
Lemma pmap_update_permute : ∀X (m : pmap X) x y v w,
    x ≠ y →
    m & { x --> v } & { y --> w } = m & { y --> w } & { x --> v }.
Proof.
  intros X m x y v w H.
  apply map_update_permute.
  assumption.
Qed.

Close Scope pmap_notation_scope.

(* Fin de PMaps.v *)
