Require Import Utf8 Nat EqNat Bool List String.
Import ListNotations.
Require Import Ids Maps PMaps Gram Eval Hoare.

Set Implicit Arguments.

Section Factorial.

  Definition x := Id "x".
  Definition fact := Id "factorial".

  Definition factorial := Fun fact x
                              (PIf (BEq x 0)
                                   1
                                   (AMult x (PApp fact (APred x)))).

  Open Scope map_notation_scope.
  Open Scope pmap_notation_scope.

  Definition amb : ambiente_f := { --> } & { fact --> factorial }.

  Notation "p '/' a '\\' n" := (@eval amb p a n)
    (at level 40, n at level 39).
  Notation "e '/' a '==>' m" := (@eval_arit amb e a m)
    (at level 40, m at level 39).
  Notation "b '/' a '-->' b'" := (@eval_bool amb b a b')
    (at level 40, b' at level 39).

  Example ex1 : (PApp fact 6) / { --> 0 } \\ 720.
  Proof.
    econstructor.
    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (6 =? 0).
    try repeat constructor.
    reflexivity.
    replace 720 with (6 * 120).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (5 =? 0).
    try repeat constructor.
    reflexivity.
    replace 120 with (5 * 24).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (4 =? 0).
    try repeat constructor.
    reflexivity.
    replace 24 with (4 * 6).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (3 =? 0).
    try repeat constructor.
    reflexivity.
    replace 6 with (3 * 2).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (2 =? 0).
    try repeat constructor.
    reflexivity.
    replace 2 with (2 * 1).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_False.
    replace false with (1 =? 0).
    try repeat constructor.
    reflexivity.
    replace 1 with (1 * 1).
    try repeat constructor.
    econstructor.

    unfold amb.
    apply pmap_update_eq.
    simpl.
    intuition.
    try repeat constructor.
    eapply E_PIf_True.
    replace true with (0 =? 0).
    try repeat constructor.
    reflexivity.
    try repeat constructor.

    reflexivity.
    reflexivity.
    reflexivity.
    reflexivity.
    reflexivity.
    reflexivity.
  Qed.

  Fixpoint fact_coq (n : nat) :=
    match n with
    | 0 => 1
    | S m => n * fact_coq m
    end.

  Lemma factorial_cerrada : cerrada factorial.
  Proof.
    unfold cerrada, factorial; simpl; intros.
    repeat destruct H; trivial.
  Qed.
  
  Theorem factorial_fact_coq :
    anotacion_func amb (λ _, True) factorial (λ n m, m = fact_coq n).
  Proof.
    (*unfold aotacion_func, factorial; intros.*)
    apply hoare_func; intros.
    apply factorial_cerrada.
    simpl; intros.
    apply hoare_if with (λ b a, a x =? 0 = b).
    - (* guardia *)
      apply hoare_eq with
          (Q1 := λ n a, n = a x)
          (Q2 := λ n _, n = 0).
      eapply hoare_consecuencia with (Q' := λ n a, n = a x).
      apply hoare_ea, hoare_var.
      unfold hoare_implies; trivial.
      unfold hoare_implies; trivial.
      eapply hoare_consecuencia with (Q' := λ n _, n = 0).
      apply hoare_ea, hoare_num.
      unfold hoare_implies; trivial.
      unfold hoare_implies; trivial.
      auto.
    - (* la guardia es verdadera *)
      eapply hoare_consecuencia with (Q' := λ m a, m = fact_coq (a x)).
      apply hoare_ea, hoare_num.
      unfold hoare_implies; intros a [_ Heq].
      apply beq_nat_true in Heq; rewrite Heq.
      reflexivity.
      unfold hoare_implies; trivial.
    - (* la guardia es falsa *)
      apply hoare_ea, hoare_prod with
          (Q1 := λ m a, m = a x ∧ m ≠ 0)
          (Q2 := λ m a, m = fact_coq (pred (a x))).
      eapply hoare_consecuencia with (Q' := λ m a, m = a x ∧ m ≠ 0).
      apply hoare_ea, hoare_var.
      unfold hoare_implies; intros a [_ Hneq].
      apply beq_nat_false in Hneq.
      auto.
      unfold hoare_implies; trivial.
      (* llamada recursiva *)
      apply hoare_app with (F := factorial)
                           (Pre := λ _, True)
                           (Post := λ n m, m = fact_coq n)
                           (R := λ m a, m = pred (a x)).
      reflexivity.
      unfold anotacion_func, factorial.
      trivial.
      apply hoare_ea, hoare_pred.
      eapply hoare_consecuencia with (Q' := λ n a, pred n = pred (a x)).
      apply hoare_ea, hoare_var.
      unfold hoare_implies; trivial.
      unfold hoare_implies; trivial.
      trivial.
      intros; subst; auto.
      intros n m a [H1 Hneq0] H2; subst.
      destruct (a x);
      [ contradiction
      | reflexivity ].
  Qed.

  Close Scope map_notation_scope.
  Close Scope pmap_notation_scope.

End Factorial.

(* Fin de Factorial.v *)
