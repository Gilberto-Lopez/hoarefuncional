Require Import Utf8 FunctionalExtensionality.
Require Import Ids.

Set Implicit Arguments.

Declare Scope map_notation_scope.

(* Funciones totales, o Maps. Funciones totales de tipo ∀ X, id → X. *)
Definition map (X : Type) := id → X.

(* Funcion constante v, v : X. *)
Definition map_const {X : Type} (v : X) : map X :=
  λ _, v.

Notation "{ --> v }" := (map_const v) (at level 0)
                        : map_notation_scope.

(* Actualización del valor de la función m en x por v. *)
Definition map_update {X : Type} (m : map X) (x : id) (v : X) : map X :=
  λ x', if id_eq x x' then v else m x'.

Notation "m '&' { x --> v }" := (map_update m x v) (at level 20)
                                : map_notation_scope.

Open Scope map_notation_scope.

(* La funcion constante regresa siempre la misma constante. *)
Lemma map_apply_const : ∀ (X : Type) x (v : X),
    { --> v } x = v.
Proof.
  intros X x v.
  reflexivity.
Qed.

(* Actualizar una función en x y aplicarla en x resulta en el nuevo valor. *)
Lemma map_update_eq : ∀X (m : map X) x v,
    (m & { x --> v }) x = v.
Proof.
  intros X m x v.
  unfold map_update.
  rewrite id_eq_refl.
  reflexivity.
Qed.

(* El valor de la función actualizada en un punto en otros puntos distintos
 * no cambia. *)
Lemma map_update_neq : ∀X (m : map X) x y v,
    x ≠ y →
    (m & { x --> v }) y = m y.
Proof.
  intros X m x y v Hneq.
  unfold map_update.
  apply id_eq_false_iff in Hneq.
  rewrite Hneq.
  reflexivity.
Qed.

(* Ahora se asume el Axioma de extensionalidad funcional. *)
(* Check functional_extensionality. *)
(* ∀ (A B : Type) (f g : A → B), (∀ x : A, f x = g x) → f = g *)

(* Actualizar el valor en un punto oculta actualizaciones previas. *)
Lemma map_update_shadow : ∀X (m : map X) x v w,
    m & { x --> v } & { x --> w } = m & { x --> w }.
Proof.
  intros.
  apply functional_extensionality.
  intro z.
  unfold map_update.
  destruct (id_eq x z); reflexivity.
Qed.

(* Actualizar el valor de la función por su valor no afecta. *)
Lemma map_update_same : ∀X (m : map X) x,
    m & { x --> m x } = m.
Proof.
  intros.
  apply functional_extensionality.
  intro y.
  destruct (id_reflect x y).
  - subst.
    apply map_update_eq.
  - apply map_update_neq.
    assumption.
Qed.

(* El orden en que se actualicen puntos distintos de la función no afecta. *)
Lemma map_update_permute : ∀X (m : map X) x y v w,
    x ≠ y →
    m & { x --> v } & { y --> w } = m & { y --> w } & { x --> v }.
Proof.
  intros X m x y v w Hneq.
  apply functional_extensionality.
  intro z.
  destruct (id_reflect x z),(id_reflect y z); subst; try contradiction.
  - rewrite map_update_eq.
    rewrite (map_update_neq _ _ n).
    apply map_update_eq.
  - rewrite map_update_eq.
    rewrite (map_update_neq _ _ Hneq).
    symmetry.
    apply map_update_eq.
  - rewrite (map_update_neq _ _ n).
    rewrite (map_update_neq _ _ n0).
    rewrite (map_update_neq _ _ n0).
    apply (map_update_neq _ _ n).
Qed.

Close Scope map_notation_scope.

(* Fin de Maps.v *)
