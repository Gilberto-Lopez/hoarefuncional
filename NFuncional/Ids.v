Require Import Utf8 Bool String.

(* Identificadores para variables. Envuelven strings que serán los nombres
 * de las variables para usarse en programas. *)
Inductive id : Type :=
| Id : string → id.

(* Igualdad entre ids. *)
Definition id_eq x y :=
  match x,y with
  | Id n1, Id n2 => if string_dec n1 n2 then true else false
  end.

(* La igualdad es reflexiva. *)
Theorem id_eq_refl : ∀x,
    id_eq x x = true.
Proof.
  intro.
  destruct x.
  unfold id_eq.
  destruct (string_dec s s);
    [reflexivity (* s = s *)
    | contradiction]. (* s ≠ s *)
Qed.

(* Pasamos de la igualdad definida a la igualdad en Coq. *)
Theorem id_eq_true_iff : ∀x y,
    id_eq x y = true ↔ x = y.
Proof.
  intros x y.
  destruct x,y.
  unfold id_eq.
  destruct (string_dec s s0).
  - subst. split; reflexivity.
  - split; intro H; inversion H.
    contradiction.
Qed.

Theorem id_eq_false_iff : ∀x y,
    id_eq x y = false ↔ x ≠ y.
Proof.
  intros x y.
  rewrite <- id_eq_true_iff.
  rewrite not_true_iff_false.
  reflexivity.
Qed.

(* Decidibilidad de la igualdad. *)
Theorem id_dec : ∀x y : id,
    {x = y} + {x ≠ y}.
Proof.
  intros x y.
  destruct x,y.
  remember (string_dec s s0) as dec eqn:H.
  destruct dec; subst.
  + left.
    reflexivity.
  + right.
    intro H'.
    inversion H'.
    contradiction.
Qed.

Theorem id_reflect : ∀x y,
    reflect (x = y) (id_eq x y).
Proof.
  intros x y.
  assert (H := id_eq_true_iff x y).
  assert (K := id_eq_false_iff x y).
  destruct (id_eq x y);
  constructor;
  [apply H | apply K];
  reflexivity.
Qed.

(* Fin de Ids.v *)
