Require Import Utf8 Nat Bool List.
Import ListNotations.
Require Import Maps Gram Eval.

Set Implicit Arguments.

Declare Scope hoare_scope.

Open Scope map_notation_scope.
Open Scope eval_notation_scope.

Section Hoare.

(* Tenemos implícitamente un ambiente global con definiciones de funciones. *)
Variable FunDefs : ambiente_f.

(* Una aserción es un enunciado lógico que describe un comportamiento en un
 * punto de la ejecución de un programa.
 * Tenemos dos tipos de aserciones
 * Rely: Son hipótesis sobre las variables libres que existen en ese punto del
 *       programa y están definidas en el ambiente actual. Son las
 *       "precondiciones" del programa en ese punto.
 * Guarantee: Son enunciados que hablan sobre el resultado de la evaluación del
 *            programa en el ambiente actual. Son las "poscondiciones" del
 *            programa en ese punto.*)
Definition rely := ambiente → Prop.
Definition guarantee := nat → ambiente → Prop.
Definition guarantee_bool := bool → ambiente → Prop.
(* Dado que el resultado de la evaluación de una expresión es un número natural
 * o un valor booleano necesitamos dos tipos de poscondiciones: guarantee
 * cuando el resultado es un número natural, y guarantee_bool cuando el
 * resultado es un valor booleano. *)

(* Un programa anotado con una aserción.
 * Las ternas de Hoare en el lenguaje Imp hablan de estados.
 * En el lenguaje funcional hablamos de valores, en este caso de naturales o
 * booleanos para las guardias. *)
Definition anotacion (P : rely) (p : programa) (Q : guarantee) : Prop :=
  ∀a n, @eval FunDefs p a n → P a → Q n a.
Definition anotacion_arit (P : rely) (e : exp_arit) (Q : guarantee) : Prop :=
  ∀a n, @eval_arit FunDefs e a n → P a → Q n a.
Definition anotacion_bool (P : rely) (g : exp_bool) (Q : guarantee_bool) : Prop :=
  ∀a b, @eval_bool FunDefs g a b → P a → Q b a.

Notation "{{ P }} p {{ Q }}" := (anotacion P p Q)
  (at level 90, p at next level) : hoare_scope.

Open Scope hoare_scope.

(* Para poder decir algo de un programa que es una expresión aritmética, es
 * necesario que se cumpla para la expresión aritmética. *)
Theorem hoare_ea : ∀e P Q,
    anotacion_arit P e Q → {{ P }} e {{ Q }}.
Proof.
  intros e P Q Harit.
  intros a n Heval HP.
  inversion Heval; subst.
  apply Harit;
  assumption.
Qed.

(* Regla (simple) para una condicional:
 * Si la guardia es verdadera se ejecuta p1, entonces se debe cumplir Q tras
 * terminar su ejecución. Si la guardia es falsa, se ejecuta p2, y se debe
 * cumplir Q tras su ejecución.
 *
 *     {{ P ∧ g --> true  }} p1 {{ Q }}
 *     {{ P ∧ g --> false }} p2 {{ Q }}
 *  --------------------------------------  (hoare_if_simple)
 *   {{ P }} if g then p1 else p2 {{ Q }}
 *
 *)
(*
Lemma hoare_if_simple : ∀g p1 p2 P Q,
    {{ λ a, P a ∧ @eval_bool FunDefs g a true  }} p1 {{ Q }} →
    {{ λ a, P a ∧ @eval_bool FunDefs g a false }} p2 {{ Q }} →
    {{ P }} PIf g p1 p2 {{ Q }}.
Proof.
  intros g p1 p2 P Q HIfTrue HIfFalse.
  intros a n Heval Hp.
  inversion Heval;
  subst;
  [ apply HIfTrue      (* La gaurdia es verdadera. *)
  | apply HIfFalse ];  (* La gaurdia es falsa. *)
  auto.
Qed.
*)

(* Regla para una expresión condicional:
 * Si la guardia es verdadera se ejecuta p1, entonces se debe cumplir Q tras
 * terminar su ejecución. Si la guardia es falsa, se ejecuta p2, y se debe
 * cumplir Q tras su ejecución. Dentro de las ramas del if tenemos una hipótesis
 * extra Q' que habla sobre el valor al que se evalúa la guardia.
 *
 *            {{ P }} g {{ Q' }}
 *      {{ P ∧ Q' true  }} p1 {{ Q }}
 *      {{ P ∧ Q' false }} p2 {{ Q }}
 *  --------------------------------------  (hoare_if)
 *   {{ P }} if g then p1 else p2 {{ Q }}
 *
 *)
Theorem hoare_if : ∀g p1 p2 P Q Q',
    anotacion_bool P g Q' →
    {{ λ a, P a ∧ Q' true  a }} p1 {{ Q }} →
    {{ λ a, P a ∧ Q' false a }} p2 {{ Q }} →
    {{ P }} PIf g p1 p2 {{ Q }}.
Proof.
  intros g p1 p2 P Q Q' Hg HIfTrue HIfFalse.
  intros a n Heval Hp.
  inversion Heval;
  subst;
  [ apply HIfTrue      (* La gaurdia es verdadera. *)
  | apply HIfFalse ];  (* La gaurdia es falsa. *)
  auto.
Qed.

(* Implicación en el nivel lógico en un punto dado del programa donde no cambia
 * el ambiente de variables. *)
Definition hoare_implies (P Q : rely) :=
  ∀a, P a → Q a.

Notation "P ->> Q" := (hoare_implies P Q) (at level 80, right associativity)
                      : hoare_scope.

(* Regla de consecuencia lógica:
 * Permite cambiar las precondiciones y/o poscondiciones en una anotación.
 *
 *   {{ P' }} p {{ Q' }} 
 *        P  ->> P'
 *        Q' ->> Q
 *  ---------------------  (hoare_consecuencia)
 *    {{ P }} p {{ Q }}
 *
 *)
Theorem hoare_consecuencia : ∀p P Q P' Q',
    {{ P' }} p {{ Q' }} →
    P ->> P' →
    (∀n, Q' n ->> Q n) →
    {{ P }} p {{ Q }}.
Proof.
  intros p P Q P' Q' H IP IQ.
  intros a m Heval HP.
  apply IQ.
  apply H.
  assumption.
  apply IP.
  assumption.
Qed.

(* Regla de consecuencia para expresiones aritméticas. *)
Theorem hoare_consecuencia_arit : ∀e P Q P' Q',
    anotacion_arit P' e Q' →
    P ->> P' →
    (∀n, Q' n ->> Q n) →
    anotacion_arit P e Q.
Proof.
  intros e P Q P' Q' H IP IQ.
  intros a m Heval HP.
  apply IQ.
  apply H.
  assumption.
  apply IP.
  assumption.
Qed.

(* Regla de consecuencia para expresiones booleanas. *)
Theorem hoare_consecuencia_bool : ∀g P (Q : guarantee_bool) P' Q',
    anotacion_bool P' g Q' →
    P ->> P' →
    (∀b a, Q' b a → Q b a) →
    anotacion_bool P g Q.
Proof.
  intros p P Q P' Q' H IP IQ.
  intros a b Heval HP.
  apply IQ.
  apply H.
  assumption.
  apply IP.
  assumption.
Qed.

(* Regla para una expresión let:
 * Si se ejecuta p1 y aseguramos R, R junto con la hipótesis general P deben
 * implicar Q en la terminación de p2.
 *
 *          {{ P }} p1 {{ R }}
 *        {{ P ∧ R }} p2 {{ Q }}
 *  ----------------------------------
 *   {{ P }} let x = p1 in p2 {{ Q }}
 *
 *)
Proposition hoare_let : ∀x p1 p2 P Q R,
    {{ P }} p1 {{ R }} →
    {{ λ a, P a ∧ R (a x) a }} p2 {{ Q }} →
    {{ P }} PLet x p1 p2 {{ Q }}.
Proof.
  intros x p1 p2 P Q R Hp1 Hp2.
  intros a m Heval Hp.
  inversion Heval; subst.
  apply Hp2.
  admit. (* El ambiente no es el correcto. *)
  split.
  assumption.
  admit. (* a x no necesariamente es n. *)
Abort. (* La regla necesita ajustes. *)

(* Regla para una expresión let:
 * Dado que durante la ejecución de p2 el ambiente es distinto, es necesario
 * hacer el cambio de ambiente explícito donde se liga la variable "x" por el
 * valor al que se evaluó p1.
 * Dentro del cuerpo del let se necesita el nuevo ambiente actualizado a', pero
 * no en la poscondición de la expresión let ya que el nombre x solo existe
 * dentro del cuerpo p2 (si x ya existía en el ambiente su valor es
 * potencialmente distinto dentro de p2).
 *
 *          {{ P }} p1 {{ R }}
 *          {{ S }} p2 {{ T }}
 *               P ∧ R → S
 *                 T → Q
 *  ----------------------------------  (hoare_let)
 *   {{ P }} let x = p1 in p2 {{ Q }}
 *
 * donde S y T requiere el nuevo ambiente a'.
 *)
Theorem hoare_let : ∀x p1 p2 P (Q : guarantee) R S T,
    {{ P }} p1 {{ R }} →
    {{ S }} p2 {{ T }} →
    (∀ n a, P a → R n a → S (a & { x --> n })) →
    (∀ n m a, T m (a & { x --> n }) → Q m a) →
    {{ P }} PLet x p1 p2 {{ Q }}.
Proof.
  intros x p1 p2 P S Q R T Hp1 Hp2 IPS ITQ.
  intros a m Heval HP.
  inversion Heval; subst.
  apply ITQ with n;
  apply Hp2;
  try assumption.
  apply IPS;
  try assumption.
  apply Hp1;
  assumption.
Qed.

(* Regla let con cuantificadores.
 * Esta regla requiere mayor expresividad del lenguaje de aserciones.
 *)
Proposition hoare_let_cuant : ∀x p1 p2 P (Q : guarantee) R,
    {{ P }} p1 {{ R }} →
    {{ λ a', ∃ a v, a' = a & { x --> v } /\ P a /\ R v a }} p2 {{ λ v' a', ∀a v, a' = a & { x --> v } -> Q v' a }} →
    {{ P }} PLet x p1 p2 {{ Q }}.
Proof.
  intros x p1 p2 P Q R Hp1 Hp2.
  intros a m Heval HP.
  inversion Heval; subst.
  cut (∀a0 v0, a & { x --> n } = a0 & { x --> v0 } -> Q m a0).
  intros.
  apply H with n;
  reflexivity.
  apply Hp2.
  assumption.
  exists a;
  exists n;
  auto.
Qed.

(* Regla para operadores binarios:
 * Dado un operador binario @, la regla general para expresiones aritméticas
 * p1 @ p2 es
 *
 *     {{ P }} p1 {{ Q1 }}
 *     {{ P }} p2 {{ Q2 }}
 *   Q1 n ∧ Q2 m → Q (n @ m)
 *  -------------------------  (hoare_<op>)
 *   {{ P }} p1 @ p2 {{ Q }}
 *
 *)
(* Suma *)
Theorem hoare_plus : ∀p1 p2 P (Q : guarantee) Q1 Q2 ,
    {{ P }} p1 {{ Q1 }} →
    {{ P }} p2 {{ Q2 }} →
    (∀n m a, Q1 n a → Q2 m a → Q (n + m) a) →
    anotacion_arit P (APlus p1 p2) Q.
Proof.
  intros p1 p2 P Q Q1 Q2 Hp1 Hp2 HI.
  intros a r Heval HP.
  inversion Heval; subst.
  apply HI;
  [ apply Hp1
  | apply Hp2];
  assumption.
Qed.

(* Producto *)
Theorem hoare_prod : ∀p1 p2 P (Q : guarantee) Q1 Q2,
    {{ P }} p1 {{ Q1 }} →
    {{ P }} p2 {{ Q2 }} →
    (∀n m a, Q1 n a → Q2 m a → Q (n * m) a) →
    anotacion_arit P (AMult p1 p2) Q.
Proof.
  intros p1 p2 P Q Q1 Q2 Hp1 Hp2 HI.
  intros a r Heval HP.
  inversion Heval; subst.
  apply HI;
  [ apply Hp1
  | apply Hp2];
  assumption.
Qed.

(* Regla para operadores unarios:
 * Dado un operador unario #, la regla general para expresiones aritméticas
 * # p es
 *
 *   {{ P }} p {{ λn.Q (# n) }}
 *  ----------------------------  (hoare_<op>)
 *    {{ P }} # p {{ λn.Q n }}
 *
 *)
(* Sucesor *)
Theorem hoare_succ : ∀p P (Q : guarantee),
    {{ P }} p {{ λ n, Q (S n) }} →
    anotacion_arit P (ASucc p) Q.
Proof.
  intros p P Q Hp.
  intros a r Heval HP.
  inversion Heval; subst.
  apply Hp;
  assumption.
Qed.

(* Predecesor *)
Theorem hoare_pred : ∀p P (Q : guarantee),
    {{ P }} p {{ λ n, Q (pred n) }} →
    anotacion_arit P (APred p) Q.
Proof.
  intros p P Q Hp.
  intros a r Heval HP.
  inversion Heval; subst.
  apply Hp;
  assumption.
Qed.

(* Reglas análogas a las reglas de expresiones aritméticas para la categoría de
 * expresiones booleanas. Los esqueletos de las reglas son los mismos, por lo
 * que las demostraciones son esencialmente iguales. *)
(* Negación *)
Theorem hoare_not : ∀g P (Q : guarantee_bool),
    anotacion_bool P g (λ b, Q (negb b)) →
    anotacion_bool P (BNot g) Q.
Proof.
  intros g P Q Hg.
  intros a b Heval HP.
  inversion Heval; subst.
  apply Hg;
  assumption.
Qed.

(* Conjunción *)
Theorem hoare_and : ∀g1 g2 P (Q : guarantee_bool) Q1 Q2 ,
    anotacion_bool P g1 Q1 →
    anotacion_bool P g2 Q2 →
    (∀b1 b2 a, Q1 b1 a → Q2 b2 a → Q (b1 && b2) a) →
    anotacion_bool P (BAnd g1 g2) Q.
Proof.
  intros g1 g2 P Q Q1 Q2 Hg1 Hg2 HI.
  intros a b Heval HP.
  inversion Heval; subst.
  apply HI;
  [ apply Hg1
  | apply Hg2 ];
  assumption.
Qed.

(* Igualdad *)
Theorem hoare_eq : ∀p1 p2 P (Q : guarantee_bool) Q1 Q2 ,
    {{ P }} p1 {{ Q1 }} →
    {{ P }} p2 {{ Q2 }} →
    (∀n m a, Q1 n a → Q2 m a → Q (n =? m) a) →
    anotacion_bool P (BEq p1 p2) Q.
Proof.
  intros p1 p2 P Q Q1 Q2 Hp1 Hp2 HI.
  intros a b Heval HP.
  inversion Heval; subst.
  apply HI;
  [ apply Hp1
  | apply Hp2 ];
  assumption.
Qed.

(* Menor que *)
Theorem hoare_lt : ∀p1 p2 P (Q : guarantee_bool) Q1 Q2 ,
    {{ P }} p1 {{ Q1 }} →
    {{ P }} p2 {{ Q2 }} →
    (∀n m a, Q1 n a → Q2 m a → Q (n <? m) a) →
    anotacion_bool P (BLt p1 p2) Q.
Proof.
  intros p1 p2 P Q Q1 Q2 Hp1 Hp2 HI.
  intros a b Heval HP.
  inversion Heval; subst.
  apply HI;
  [ apply Hp1
  | apply Hp2 ];
  assumption.
Qed.

(* Regla para constantes:
 * Dada una expresión constante C, dado que no ocurren cómputos, lo que se
 * quiera concluir debe ser algo que ya era cierto. Así la regla general es
 *
 * -------------------  (hoare_<const>)
 *  {{ P }} C {{ P }}
 *
 *)
(* Constantes numéricas *)
Theorem hoare_num : ∀n (Q : guarantee),
    anotacion_arit (Q n) (ANum n) Q.
Proof.
  intros n Q.
  intros a m Heval HQ.
  inversion Heval; subst.
  assumption.
Qed.

(* Constantes booleanas *)
Theorem hoare_bool : ∀b (Q : guarantee_bool),
    anotacion_bool (Q b) (BBool b) Q.
Proof.
  intros b Q.
  intros a b' Heval HQ.
  inversion Heval; subst.
  assumption.
Qed.

(* Regla para variables:
 * El caso de la regla para variables es igual al caso de las constantes, las
 * variables son nombres para los valores asignados por el ambiente de
 * variables en la ejecución. *)
Theorem hoare_var : ∀x (Q : guarantee),
    anotacion_arit (λ a, Q (a x) a) (AId x) Q.
Proof.
  intros x Q.
  intros a m Heval HQ.
  inversion Heval; subst.
  assumption.
Qed.

(* Precondición y poscondición de una función.
 * La especificación de una función f consiste de una precondición y una
 * poscondición, donde la precondición habla (del valor) del argumento "x" y la
 * poscondición habla (del valor) del argumento y (del valor) del resultado
 * de la evaluación de f(x).
 * Hablamos de los valores pues una función debe ser cerrada y para razonar
 * dependemos únicamente de su especificación. *)
Definition pre := nat → Prop.
Definition post := nat → nat → Prop.

(* Definición de terna para funciones. *)
Definition anotacion_func (Pre : pre) (F : funcion) (Post : post) : Prop :=
  match F with
  | Fun f x p => forall n m, @eval FunDefs p { x --> n } m -> Pre n -> Post n m
  end.

(* Regla para funciones que no se puede demostrar.
 * La regla tiene forma (A -> _) -> A. *)
Proposition hoare_func : forall Pre Post f x p,
    cerrada (Fun f x p) ->
    (anotacion_func Pre (Fun f x p) Post -> {{ fun a => Pre (a x) }} p {{ fun m a => Post (a x) m }}) ->
    anotacion_func Pre (Fun f x p) Post.
Proof.
  intros Pre Post f x p Hcf HI.
  unfold anotacion_func;
  intros n m Heval HPre.
  assert (K := map_update_eq { --> 0} x n).
  rewrite <- K.
  apply HI.
  admit. (* Para poder concluir la terna sobre f, es necesario demostrar que las
          * llamadas recursivas cumplan con la propiedad que se está tratando de
          * demostrar. *)
  assumption.
  rewrite K;
  assumption.
Abort.

(* Regla para funciones:
 * La especificación de una función, precondición Pre y poscondición Post.
 * La semántica es: dada una función cerrada, asumiendo la especificación (para
 * llamadas recursivas dentro del cuerpo), que se cumple P y que la evaluación
 * del cuerpo de la función termina, entonces la poscondición se cumple.
 * Esta es una semántica para corrección parcial, una función puede ser
 * recursiva no total, esto es, no se requiere demostrar la totalidad de la
 * función.
 *
 *   {{ Pre }} f {{ Post }} |- {{ Pre }} p {{ Post }}
 *  -------------------------------------------------- (hoare_func)
 *                {{ Pre }} f {{ Post }}
 *
 *)
Axiom hoare_func : ∀Pre F Post,
    cerrada F ->
    match F with
    | Fun _ x p => anotacion_func Pre F Post → {{ λ a, Pre (a x) }} p {{ λ m a, Post (a x) m }}
    end
    -> anotacion_func Pre F Post.

(* Regla para aplicación de funciones:
 * Dado un identificador de función f, para el cual hay una función definida en
 * el ambiente de funciones, la regla de corrección parcial para aplicación de
 * funciones es
 *
 *   {{ Pre  }} f {{ Post }}
 *      {{ P }} x {{ R }}
 *           R → Pre
 *        R ∧ Post → Q
 *  -------------------------  (hoare_app)
 *     {{ P }} f x {{ Q }}
 *)
Theorem hoare_app : ∀F f q P (Q R : guarantee) Pre Post,
    FunDefs f = Some F →
    anotacion_func Pre F Post →
    {{ P }} q {{ R }} →
    (∀a n, R n a → Pre n) →
    (∀a n m, R n a → Post n m → Q m a) →
    {{ P }} PApp f q {{ Q }}.
Proof.
  intros F f q P Q R Pre Post EqF Hf Hq HPre HPost.
  intros a m Heval Hp.
  inversion Heval;
  destruct F;
  rewrite EqF in *;
  inversion H1;
  subst;
  clear H1.
  unfold anotacion in Hq.
  apply HPost with n.
  apply Hq; assumption.
  unfold anotacion_func in Hf.
  apply Hf.
  assumption.
  apply HPre with a.
  apply Hq;
  assumption.
Qed.

Close Scope hoare_scope.

End Hoare.

Close Scope map_notation_scope.
Close Scope eval_notation_scope.

(* Fin de Hoare.v *)
