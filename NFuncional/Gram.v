Require Import Utf8 String List.
Import ListNotations.
Require Import Ids.

(* Definicion de la gramática para programas funcionales simples. *)
Inductive programa :=
| PEA : exp_arit → programa (* Solo manejamos naturales. *)
| PIf : exp_bool → programa → programa → programa
| PLet : id → programa → programa → programa
| PApp : id → programa → programa
with exp_arit :=
     | ANum : nat → exp_arit
     | AId : id → exp_arit
     | ASucc : programa → exp_arit
     | APred : programa → exp_arit
     | APlus : programa → programa → exp_arit
     | AMult : programa → programa → exp_arit
with exp_bool :=
     | BBool : bool → exp_bool
     | BEq : programa → programa → exp_bool
     | BLt : programa → programa → exp_bool
     | BNot : exp_bool → exp_bool
     | BAnd : exp_bool → exp_bool → exp_bool.

(* Una función es una caja negra que envuelve un programa.
 * Liga su nombre y el de su parametro. *)
Inductive funcion :=
| Fun : id → id → programa → funcion.

Coercion Id: string >-> id.
Coercion PEA: exp_arit >-> programa.
Coercion ANum: nat >-> exp_arit.
Coercion AId: id >-> exp_arit.

(* Función que recupera todas las instancias (en orden) de variables libres
 * de un programa.
 * El resultado es una lista, no un conjunto. *)
Fixpoint fv (p : programa) : list id :=
  match p with
  | PEA e => fv_arit e
  | PIf g p1 p2 => fv_bool g ++ fv p1 ++ fv p2
  | PLet x p1 p2 => fv p1 ++ (remove id_dec x (fv p2))
  | PApp _ q => fv q
  end
with fv_arit (e : exp_arit) : list id :=
       match e with
       | AId x => [x]
       | ASucc p => fv p
       | APred p => fv p
       | APlus p1 p2 => fv p1 ++ fv p2
       | AMult p1 p2 => fv p1 ++ fv p2
       | _ => []
       end
with fv_bool (g : exp_bool) : list id :=
       match g with
       | BEq p1 p2 => fv p1 ++ fv p2
       | BLt p1 p2 => fv p1 ++ fv p2
       | BNot g => fv_bool g
       | BAnd g1 g2 => fv_bool g1 ++ fv_bool g2
       | _ => []
       end.

(* Función que recupera todas las instancias (en orden) de variables libres
 * de funciones de un programa.
 * El resultado es una lista, no un conjunto. *)
Fixpoint ffv (p : programa) : list id :=
  match p with
  | PEA e => ffv_arit e
  | PIf g p1 p2 => ffv_bool g ++ ffv p1 ++ ffv p2
  | PLet x p1 p2 => ffv p1 ++ ffv p2
  | PApp f q => f :: ffv q
  end
with ffv_arit (e : exp_arit) : list id :=
       match e with
       | ASucc p => ffv p
       | APred p => ffv p
       | APlus p1 p2 => ffv p1 ++ ffv p2
       | AMult p1 p2 => ffv p1 ++ ffv p2
       | _ => []
       end
with ffv_bool (g : exp_bool) : list id :=
       match g with
       | BEq p1 p2 => ffv p1 ++ ffv p2
       | BLt p1 p2 => ffv p1 ++ ffv p2
       | BNot g => ffv_bool g
       | BAnd g1 g2 => ffv_bool g1 ++ ffv_bool g2
       | _ => []
       end.

(* Predicados que determinan si un programa, una expresión aritmética o una
 * expresión booleana es una expresión cerrada, i.e., no tiene variables
 * libres. *)
Definition cerrado p : Prop :=
  fv p = [].
Definition cerrada_arit e : Prop :=
  fv_arit e = [].
Definition cerrada_bool g : Prop :=
  fv_bool g = [].

(* Predicado que determina si una función es cerrada, i.e., su cuerpo habla
 * únicamente del parámetro formal (x). *)
Definition cerrada f : Prop :=
  match f with
  | Fun _ x p => ∀y, In y (fv p) → x = y
  end.

(* Fin de Gram.v *)
