Require Import Utf8 Bool Nat List.
Import ListNotations.
Require Import Ids Maps PMaps Gram.

Set Implicit Arguments.

Declare Scope eval_notation_scope.

(* Un ambiente guarda el valor de las variables durante la ejecución de un
 * programa, con esto evitamos generar código al vuelo con sustituciones al
 * implementar la evaluación. *)
Definition ambiente := map nat.

(* El tipo ambiente_f es un ambiente que guarda las definiciones de las
 * funciones. *)
Definition ambiente_f := pmap funcion.

Open Scope map_notation_scope.

Notation "{ x --> v }" := ({ --> 0 } & { x --> v }) (at level 0)
                          : eval_notation_scope.

Open Scope eval_notation_scope.

Reserved Notation "p '/' a '\\' n" (at level 40, n at level 39).
Reserved Notation "e '/' a '==>' m" (at level 40, m at level 39).
Reserved Notation "g '/' a '-->' b" (at level 40, b at level 39).

(* Semántica operacional de paso grande. *)
Inductive eval { ambf : ambiente_f } : programa → ambiente → nat → Prop :=
| E_PEA : ∀e a n,
    e / a ==> n →
    PEA e / a \\ n
| E_PIf_True : ∀g p1 p2 a n,
    g / a --> true →
    p1 / a \\ n →
    PIf g p1 p2 / a \\ n
| E_PIf_False : ∀g p1 p2 a n,
    g / a --> false →
    p2 / a \\ n →
    PIf g p1 p2 / a \\ n
| E_PLet : ∀x p1 p2 a n m,
    p1 / a \\ n →
    p2 / a & { x --> n } \\ m →
    PLet x p1 p2 / a \\ m
| E_PApp : ∀f x p q a n m,
    ambf f = Some (Fun f x p) →
    cerrada (Fun f x p) →
    q / a \\ n →
    p / { x --> n } \\ m →
    PApp f q / a \\ m
where "p '/' a '\\' n" := (eval p a n) : eval_notation_scope

with eval_arit { ambf : ambiente_f } : exp_arit → ambiente → nat → Prop :=
     | E_ANum : ∀n a,
         ANum n / a ==> n
     | E_AId : ∀x a,
         AId x / a ==> a x
     | E_ASucc : ∀p a n,
         p / a \\ n →
         ASucc p / a ==> (S n)
     | E_APred : ∀p a n,
         p / a \\ n →
         APred p / a ==> pred n
     | E_APlus : ∀p1 p2 a n m,
         p1 / a \\ n →
         p2 / a \\ m →
         APlus p1 p2 / a ==> (n + m)
     | E_AMult : ∀p1 p2 a n m,
         p1 / a \\ n →
         p2 / a \\ m →
         AMult p1 p2 / a ==> (n * m)
where "e '/' a '==>' m" := (eval_arit e a m) : eval_notation_scope

with eval_bool { ambf : ambiente_f } : exp_bool → ambiente → bool → Prop :=
     | B_BBool : ∀b a,
         BBool b / a --> b
     | B_BEq : ∀p1 p2 a n m,
         p1 / a \\ n →
         p2 / a \\ m →
         BEq p1 p2 / a  --> (n =? m)
     | B_BLt : ∀p1 p2 a n m,
         p1 / a \\ n →
         p2 / a \\ m →
         BLt p1 p2 /a --> (n <? m)
     | B_BNot : ∀g a b,
         g / a --> b →
         BNot g / a --> negb b
     | B_BAnd : ∀g1 g2 a b1 b2,
         g1 / a --> b1 →
         g2 / a --> b2 →
         BAnd g1 g2 / a --> andb b1 b2
where "g '/' a '-->' b" := (eval_bool g a b) : eval_notation_scope.

(* Principios de inducción mutuamente definidos. *)
Scheme eval_ind_2 := Minimality for eval Sort Prop
  with eval_arit_ind_2 := Minimality for eval_arit Sort Prop
  with eval_bool_ind_2 := Minimality for eval_bool Sort Prop.

(* Principio de inducción que junta los principios de inducción
 * eval_ind_2
 * eval_arit_ind_2
 * eval_bool_ind_2 *)
Lemma eval_mutual_ind
  : ∀ (ambf : ambiente_f) (P : programa → ambiente → nat → Prop)
      (P0 : exp_arit → ambiente → nat → Prop) (P1 :
                                                 exp_bool
                                                 →
                                                 ambiente → bool → Prop),
    (∀ (e : exp_arit) (a : ambiente) (n : nat), @eval_arit ambf e a n
                                                  → P0 e a n → P e a n)
    → (∀ (g : exp_bool) (p1 p2 : programa) (a : ambiente) (n : nat),
          @eval_bool ambf g a true
          → P1 g a true → @eval ambf p1 a n → P p1 a n → P (PIf g p1 p2) a n)
    → (∀ (g : exp_bool) (p1 p2 : programa) (a : ambiente) (n : nat),
          @eval_bool ambf g a false
          → P1 g a false → @eval ambf p2 a n → P p2 a n → P (PIf g p1 p2) a n)
    → (∀ (x : id) (p1 p2 : programa) (a : ambiente) (n m : nat),
          @eval ambf p1 a n
          → P p1 a n
          → @eval ambf p2 (a & {x --> n}) m
          → P p2 (a & {x --> n}) m → P (PLet x p1 p2) a m)
    → (∀ (f3 x : id) (p y : programa) (a : ambiente) (n m : nat),
          ambf f3 = Some (Fun f3 x p)
          → cerrada (Fun f3 x p)
          → @eval ambf y a n
          → P y a n
          → @eval ambf p {x --> n} m
          → P p {x --> n} m → P (PApp f3 y) a m)
    → (∀ (n : nat) (a : ambiente), P0 n a n)
    → (∀ (x : id) (a : ambiente), P0 x a (a x))
    → (∀ (p : programa) (a : ambiente) (n : nat),
          @eval ambf p a n → P p a n → P0 (ASucc p) a (S n))
    → (∀ (p : programa) (a : ambiente) (n : nat),
          @eval ambf p a n → P p a n → P0 (APred p) a (pred n))
    → (∀ (p1 p2 : programa) (a : ambiente) (n m : nat),
          @eval ambf p1 a n
          → P p1 a n
          → @eval ambf p2 a m
          → P p2 a m → P0 (APlus p1 p2) a (n + m))
    → (∀ (p1 p2 : programa) (a : ambiente) (n m : nat),
          @eval ambf p1 a n
          → P p1 a n
          → @eval ambf p2 a m
          → P p2 a m → P0 (AMult p1 p2) a (n * m))
    → (∀ (b : bool) (a : ambiente), P1 (BBool b) a b)
    → (∀ (p1 p2 : programa) (a : ambiente) (n m : nat),
          @eval ambf p1 a n
          → P p1 a n
          → @eval ambf p2 a m
          → P p2 a m → P1 (BEq p1 p2) a (n =? m))
    → (∀ (p1 p2 : programa) (a : ambiente) (n m : nat),
          @eval ambf p1 a n
          → P p1 a n
          → @eval ambf p2 a m
          → P p2 a m → P1 (BLt p1 p2) a (n <? m))
    → (∀ (b : exp_bool) (a : ambiente) (b' : bool),
          @eval_bool ambf b a b'
          → P1 b a b' → P1 (BNot b) a (negb b'))
    → (∀ (b1 b2 : exp_bool)
         (a : ambiente) (b1' b2' : bool),
          @eval_bool ambf b1 a b1'
          → P1 b1 a b1'
          → @eval_bool ambf b2 a b2'
          → P1 b2 a b2'
          → P1 (BAnd b1 b2) a (b1' && b2'))
    → (∀ (p : programa) (a : ambiente) (n : nat),
          @eval ambf p a n → P p a n) ∧
      (∀ (e : exp_arit) (a : ambiente) (n : nat),
          @eval_arit ambf e a n → P0 e a n) ∧
      (∀ (e : exp_bool) (a : ambiente) (b : bool),
          @eval_bool ambf e a b → P1 e a b).
Proof.
  intros. split; try split.
  apply (eval_ind_2 P P0 P1); assumption.
  apply (eval_arit_ind_2 P P0 P1); assumption.
  apply (eval_bool_ind_2 P P0 P1); assumption.
Qed.

(* La relación de evaluación es determinista con respecto al resultado de la
 * evaluación de un programa, expresión aritmética o expresión booleana.
 * Dado un programa y un ambiente de variables, el resultado siempre es el
 * mismo. *)
Theorem eval_determinista : ∀ambf,
    (∀ (p : programa) (a : ambiente) (n : nat),
        @eval ambf p a n → (∀m, @eval ambf p a m → n = m)) ∧
    (∀ (e : exp_arit) (a : ambiente) (n : nat),
        @eval_arit ambf e a n → (∀m, @eval_arit ambf e a m → n = m)) ∧
    (∀ (e : exp_bool) (a : ambiente) (b : bool),
        @eval_bool ambf e a b → (∀b', @eval_bool ambf e a b' → b = b')).
Proof.
  intro.
  apply eval_mutual_ind; intros.
  -apply H0.
   inversion H1.
   assumption.
  -apply H2.
   inversion H3; subst.
   assumption.
   apply H0 in H9.
   inversion H9.
  -apply H2.
   inversion H3; subst.
   apply H0 in H9.
   inversion H9.
   assumption.
  -apply H2.
   inversion H3; subst.
   apply H0 in H9.
   rewrite H9 in *.
   assumption.
  -apply H4.
   inversion H5; subst.
   rewrite H in H8.
   inversion H8.
   rewrite H7 in *.
   apply H2 in H10.
   rewrite H10 in *.
   assumption.
  -inversion H; subst.
   trivial.
  -inversion H; subst.
   trivial.
  -inversion H1; subst.
   apply H0 in H3.
   rewrite H3.
   trivial.
  -inversion H1; subst.
   apply H0 in H3.
   rewrite H3.
   trivial.
  -inversion H3; subst.
   apply H0 in H6.
   apply H2 in H9.
   rewrite H6,H9.
   trivial.
  -inversion H3; subst.
   apply H0 in H6.
   apply H2 in H9.
   rewrite H6,H9.
   trivial.
  -inversion H; subst.
   trivial.
  -inversion H3; subst.
   apply H0 in H6.
   apply H2 in H9.
   rewrite H6,H9.
   trivial.
  -inversion H3; subst.
   apply H0 in H6.
   apply H2 in H9.
   rewrite H6,H9.
   trivial.
  -inversion H1; subst.
   apply H0 in H3.
   rewrite H3.
   trivial.
  -inversion H3; subst.
   apply H0 in H6.
   apply H2 in H9.
   rewrite H6,H9.
   trivial.
Qed.

Close Scope map_notation_scope.
Close Scope eval_notation_scope.

(* Fin de Eval.v *)
