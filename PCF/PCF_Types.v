(* Tipos para el lenguaje PCF.
 * Los tipos son:
 * - N los números naturales
 * - B constantes booleanas
 * - * -> * El constructor de tipos función. *)

(* Tipos del lenguaje definidos como un tipo inductivo. *)
Inductive typ : Set :=
| typ_nat   : typ
| typ_bool  : typ
| typ_arrow : typ -> typ -> typ.

(* Notaciones. *)
Notation "'N'" := (typ_nat).
Notation "'B'" := (typ_bool).
Notation "U '==>' T" := (typ_arrow U T) (at level 2, right associativity).

(* Reflexión lógica de los tipos del lenguaje PCF en el sistema de tipos de Coq.
 * Para las funciones la reflexión lógica es el tipo de la especificación. *)
Reserved Notation "[ X ]".

Fixpoint typ_refl (X : typ) : Type :=
  match X with
  | N       => nat
  | B       => bool
  | U ==> T => ([U] -> Prop) * ([U] -> [T] -> Prop)
  end

where "[ X ]" := (typ_refl X).

(* Fin de PCF_Types.v *)
