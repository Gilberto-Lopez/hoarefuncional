(* Definicion del lenguaje PCF usando la Representación Local Anónima. *)

Require Import Nat.
From TLC Require Import LibLN.
Require Import PCF_Types.

(* Expresiones del lenguaje.
 * Se hace distinción sintáctica entre expresiones y valores.
 * Los términos del lenguaje `trm` son:
 * - Variables ligadas
 * - Variables libres
 * - Valores
 * - Aplicación de funciones
 * - Suma
 * - Producto
 * - Sucesor
 * - Predecesor
 * - Test IsZero
 * - Expresión condicional If
 * Los valores del lenguaje `val` son:
 * - Números naturales
 * - Constante booleana tt (true)
 * - Constante booleana ff (false)
 * - Abstracción lambda
 * - Funciones recursivas `fix`.
 * Las funciones (abstracciones y recursivas/fix) estan anotadas expícitamente
 * con los tipos y su especificación.
 * Las variables, ambas libres y ligadas, están anotadas explicitamente con su
 * tipo.
 * trm_fix debe considerarse como una abstracción que liga dos variables, el
 * nombre de la funcón misma (la variable ligada 1) y la variable parámetro
 * (la variable ligada 0). *)
Inductive trm : Type :=
| trm_bvar : nat -> trm
| trm_fvar : var -> trm
| trm_val  : val -> trm
| trm_app  : trm -> trm -> trm
| trm_plus : trm -> trm -> trm
| trm_prod : trm -> trm -> trm
| trm_suc  : trm -> trm
| trm_pred : trm -> trm
| trm_iz   : trm -> trm
| trm_if   : trm -> trm -> trm -> trm
with val : Type :=
     | trm_num  : nat -> val
     | trm_tt   : val
     | trm_ff   : val
     | trm_abs  : forall (U T : typ), ([U] -> Prop) -> ([U] -> [T] -> Prop) -> trm -> val
     | trm_fix  : forall (U T : typ), ([U] -> Prop) -> ([U] -> [T] -> Prop) -> trm -> val.

Coercion trm_val  : val >-> trm.
Coercion trm_num  : nat >-> val.

Notation "'tt'" := (trm_tt).
Notation "'ff'" := (trm_ff).

(* Función recursiva para abrir una variable ligada `k` dentro un término `t`
 * con otro término `u`. *)
Fixpoint open_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i     => if k =? i then u else t
  | trm_fvar _     => t
  | trm_val v      => trm_val (open_rec_val k u v)
  | trm_app t1 t2  => trm_app (open_rec k u t1) (open_rec k u t2)
  | trm_plus t1 t2 => trm_plus (open_rec k u t1) (open_rec k u t2)
  | trm_prod t1 t2 => trm_prod (open_rec k u t1) (open_rec k u t2)
  | trm_suc t1     => trm_suc (open_rec k u t1)
  | trm_pred t1    => trm_pred (open_rec k u t1)
  | trm_iz t1      => trm_iz (open_rec k u t1)
  | trm_if t1 t2 t3 => trm_if (open_rec k u t1) (open_rec k u t2) (open_rec k u t3)
  end
with open_rec_val (k : nat) (u : trm) (v : val) {struct v} : val :=
       match v with
       | trm_abs U T P Q t1  => trm_abs U T P Q (open_rec (S k) u t1)
       | trm_fix U T P Q t1  => trm_fix U T P Q (open_rec (S (S k)) u t1)
       | _                   => v
       end.

Notation "{ k ~> u } t" := (open_rec k u t) (at level 67, right associativity).

Definition open t u := {0 ~> u} t.
Definition open_t t s u := {0 ~> u} {1 ~> s} t.

Notation "t ^^ u" := (open t u) (at level 67).

(* Predicado que determina si una expresión es un término del lenguaje.
 * No debe haber variables ligadas que apuntan a abstracciones inexistentes. *)
Inductive term : trm -> Prop :=
| term_var : forall x,
    term (trm_fvar x)
| term_v : forall v,
    term_val v -> term v
| term_app : forall t1 t2,
    term t1 -> 
    term t2 -> 
    term (trm_app t1 t2)
| term_plus : forall t1 t2,
    term t1 -> 
    term t2 -> 
    term (trm_plus t1 t2)
| term_prod : forall t1 t2,
    term t1 -> 
    term t2 -> 
    term (trm_prod t1 t2)
| term_suc : forall t1,
    term t1 -> 
    term (trm_suc t1)
| term_pred : forall t1,
    term t1 -> 
    term (trm_pred t1)
| term_iz : forall t1,
    term t1 -> 
    term (trm_iz t1)
| term_if : forall t1 t2 t3,
    term t1 ->
    term t2 -> 
    term t3 ->
    term (trm_if t1 t2 t3)
with term_val : val -> Prop :=
     | term_num : forall n,
         term_val (trm_num n)
     | term_tt :
         term_val tt
     | term_ff :
         term_val ff
     | term_abs : forall L U T P Q t1,
         (forall x, x \notin L -> term (t1 ^^ trm_fvar x)) ->
         term_val (trm_abs U T P Q t1)
     | term_fix : forall L U T P Q t1,
         (forall f, f \notin L ->
          forall x, x \notin L \u \{f} ->
                    term (open_t t1 (trm_fvar f) (trm_fvar x))) ->
         term_val (trm_fix U T P Q t1).

(* Fin de PCF_Lang.v *)
