From TLC Require Import LibLN.
Require Import PCF_Types PCF_Lang PCF_Eval PCF_Typing PCF_ValRefl.

Set Implicit Arguments.

Declare Scope pcf_hoare_scope.

(* Son las hipótesis sobre las variables libres que figuran en el cuerpo de un
 * programa y que tienen un valor asociado en un ambiente de evaluación. *)
Definition rely := eval_env -> Prop.
(* Son enunciados que hablan sobre el resultado de evaluar un programa, esto es,
 * el valor al que se reduce el programa. *)
Definition guarantee := forall T, [T] -> Prop.

(* Relación de congruencia entre ambientes.
 * Decimos que un ambiente de tipado G y un ambiente de evaluación E son
 * congruentes si siempre que una variable tenga un tipo T en el contexto de
 * tipado G y tenga asociado un valor v en el ambiente de evaluación E, entonces
 * el valor v tiene tipo T bajo G. *)
Definition congruente (G : ctx) (E : eval_env) :=
  forall x T T' v,
    binds x v E ->
    G |= trm_fvar x ~: T ->
    G |= v ~: T' ->
    T = T'.

(* Preservación de tipos. *)
Axiom preservation : forall G E t v T,
  G |= t ~: T ->
  E ||- t ⇓ v ->
  G |= v ~: T.

(* Dado un programa t correctamente tipado, si este se evalúaa a un valor v,
 * entonces siempre que se cumpla la _precondición_ P, se debe cumplir la
 * _poscondición_ Q. *)
Definition anotacion (G : ctx) (P : rely) (t : trm) (Q : guarantee) : Prop :=
  forall E, congruente G E ->
    forall T v (p : G |= t ~: T) (q : E ||- t ⇓ v), P E ->
      Q T (val_refl (preservation p q)).

Notation "G |- {{ P }} t {{ Q }}" := (anotacion G P t Q)
  (at level 90, t at next level) : pcf_hoare_scope.

Open Scope pcf_hoare_scope.

Theorem hoare_num : forall (G : ctx) (n : nat) (Q : guarantee),
    G |- {{ fun _ => Q N n }} trm_num n {{ Q }}.
Proof.
  intros G n Q.
  unfold anotacion; intros E Hc T v Htyping Heval HQ. (* T v Htyping Heval HQ.*)
  inversion Htyping; inversion X; subst.
  inversion Heval; subst.
  (*Check (preservation Htyping Heval).*)
  (*
    preservation Htyping Heval
     : G |= n ~: N
   *)
  replace (val_refl (preservation Htyping Heval)) with n.
  exact HQ.
  (* preservation es un axioma, así que preservation Htyping Heval tiene el tipo
   * deseado, pero no tiene contenido computacional, por lo que no se reduce a
   * n. *)
Admitted.

Theorem hoare_if : forall G t1 t2 t3 P Q R,
    G |- {{ P }} t1 {{ R }} ->
    G |- {{ fun E => P E /\ R B true }} t2 {{ Q }} ->
    G |- {{ fun E => P E /\ R B false }} t3 {{ Q }} ->
    G |- {{ P }} trm_if t1 t2 t3 {{ Q }}.
Proof.
  intros G t1 t2 t3 P Q R.
  intros Ht1 Ht2 Ht3.
  unfold anotacion; intros E Hc T v Htyping Heval HP.
  inversion Heval; subst.
  - (* La guardia es tt. *)
    inversion Htyping; subst.
    unfold anotacion in Ht2.
    (*apply (Ht2 _ Hc _ _ X0 H5).*)
    (* Error: In environment
       Unable to unify "Q T (val_refl (preservation X0 H5))" with
       "Q T (val_refl (preservation Htyping Heval))".
     *)
    replace (val_refl (preservation Htyping Heval)) with
                           (val_refl (preservation X0 H5)).
    apply Ht2; try assumption; split.
    exact HP.
    unfold anotacion in Ht1.
    (* Axioma preservation estorba de nuevo. Los pasos que siguen, y pasos
     * que sigan deberán desaparecer tras demostrar preservation. *)
    assert (Ht1_ := Ht1 _ Hc _ _ X H4 HP).
    replace (val_refl (preservation X H4)) with true in Ht1_.
    exact Ht1_.
    admit.
    admit.
  - (* La guardia es ff. *)
    inversion Htyping; subst.
    unfold anotacion in Ht3.
    (*apply (Ht3 _ Hc _ _ X1 H5).*)
    (* Error: In environment
       Unable to unify "Q T (val_refl (preservation X1 H5))" with
       "Q T (val_refl (preservation Htyping Heval))".
     *)
    replace (val_refl (preservation Htyping Heval)) with
                           (val_refl (preservation X1 H5)).
    apply Ht3; try assumption; split.
    exact HP.
    unfold anotacion in Ht1.
    (* Axioma preservation estorba de nuevo. Los pasos que siguen, y pasos
     * que sigan deberán desaparecer tras demostrar preservation. *)
    assert (Ht1_ := Ht1  _ Hc _ _ X H4 HP).
    replace (val_refl (preservation X H4)) with false in Ht1_.
    exact Ht1_.
    admit.
    admit.
Admitted.

Theorem hoare_tt : forall (G : ctx) (Q : guarantee),
    G |- {{ fun _ => Q B true }} tt {{ Q }}.
Proof.
  intros G Q.
  unfold anotacion; intros E Hc T v Htyping Heval HQ.
  inversion Htyping; inversion X; subst.
  inversion Heval; subst.
  replace (val_refl (preservation Htyping Heval)) with true.
  exact HQ.
  (* tt se ve como el término trm_val trm_tt y no como el valor.
   * preservation Htyping Heval regresa algo de tipo Htyping mismo. *)
  (*Check (preservation Htyping Heval).*)
  (* preservation Htyping Heval
          : G |= tt ~: B *)
  (* val_refl Htyping no puede realizar cómputos dado que Htyping no es
   * un valor concreto pero debe regresar true cuando usa un valor concreto, por
   * ejemplo: typing_v (typing_tt G)  (para un contexto G arbitrario, no
   * necesariamente contreto). *)
  (*Check (typing_v _ _ _ (typing_tt G)).*)
  (* typing_v G tt B (typing_tt G)
          : G |= tt ~: B *)
  (*Compute (val_refl (typing_v _ _ _ (typing_tt G))).*)
  (* = true
     : [B] *)
  (* NOTA: Revisar definición de la función val_refl. La demostración no
   * debería depender de preservation pues tt se reduce a sí mismo (ver
   * reds_val). *)
Admitted.

Theorem hoare_ff : forall (G : ctx) (Q : guarantee),
    G |- {{ fun _ => Q B false }} ff {{ Q }}.
Admitted.
(* Demostración completamente análoga a la demostración del teorema hoare_tt. *)

Theorem hoare_var : forall (G : ctx) (x : var) (T : typ) (Q : guarantee),
    G |- {{ fun E => exists (v : val), exists (p : G |= v ~: T), get x E = Some v /\ Q T (val_refl p) }} trm_fvar x {{ Q }}.
Proof.
  intros.
  unfold anotacion.
  intros E Hc T' v Htyping Heval Hex.

  inversion Heval.
  unfold binds in H1; subst.

  unfold congruente in Hc.

  destruct Hex as [v' [p [Hi Hj]]].
  unfold binds in H1.
  rewrite Hi in H1.
  inversion H1; subst; clear H1.

  assert (s := Hc _ _ _ _ Hi Htyping p).
  subst.
  (*Check (val_refl p).*)
  (* val_refl p
       : [T] *)
  (*Check (val_refl (preservation Htyping Heval)).*)
  (* val_refl (preservation Htyping Heval)
       : [T] *)
Admitted.

Theorem hoare_plus : forall G t1 t2 P (Q : guarantee) Q1 Q2,
    G |- {{ P }} t1 {{ Q1 }} ->
    G |- {{ P }} t2 {{ Q2 }} ->
    (forall n m, Q1 N n -> Q2 N m -> Q N (n + m)) ->
    G |- {{ P }} trm_plus t1 t2 {{ Q }}.
Proof.
  intros G t1 t2 P Q Q1 Q2 Ht1 Ht2 HI.
  intros E Hc T v Htyping Heval HP.
  inversion Htyping; subst.
  inversion Heval; subst.

  replace (val_refl (preservation Htyping Heval)) with (n + m).
  2:{ admit. }
  (* La regla de evaluación reds_plus sustituye el valor v en Heval por n + m
   * al realizar la inversión (y sustituciones). *)

  apply HI.
  - (* Primer operando t1*)
    unfold anotacion in Ht1.
    assert (Ht1_ := Ht1 _ Hc _ _ X H2 HP).
    replace (val_refl (preservation X H2)) with n in Ht1_.
    exact Ht1_.
    admit.
    (* Aquí, dado que Ht1 usa val_refl y preservation es necesario instanciar
     * una hipótesis con los parámetros que tenemos en el contexto de la
     * demostración. Coq debería poder simplificar Ht1_ si preservation
     * tuviera contenido computacional para tener una instancia de Q1 N n.
     * Con esto tendríamos la aseción Q1 sobre n (resultado de t1) como
     * hipótesis en el contexto y poder demostrar este caso. *)
  - (* Segundo operando p2 *)
    unfold anotacion in Ht2.
    assert (Ht2_ := Ht2 _ Hc _ _ X0 H4 HP).
    replace (val_refl (preservation X0 H4)) with m in Ht2_.
    exact Ht2_.
    admit.
    (* El caso del segundo operando es análogo al del primer operando. *)
Admitted.

Theorem hoare_prod : forall G t1 t2 P (Q : guarantee) Q1 Q2,
    G |- {{ P }} t1 {{ Q1 }} ->
    G |- {{ P }} t2 {{ Q2 }} ->
    (forall n m, Q1 N n -> Q2 N m -> Q N (n * m)) ->
    G |- {{ P }} trm_prod t1 t2 {{ Q }}.
Admitted.
(* Demostración completamente análoga a la demostración del teorema hoare_plus. *)

Theorem hoare_suc : forall G t1 P (Q : guarantee) Q',
    G |- {{ P }} t1 {{ Q' }} ->
    (forall n, Q' N n -> Q N (S n)) ->
    G |- {{ P }} trm_suc t1 {{ Q }}.
Proof.
  intros G t1 P Q Q' Ht1 HI.
  intros E Hc T v Htyping Heval HP.
  inversion Htyping; subst.
  inversion Heval; subst.

  replace (val_refl (preservation Htyping Heval)) with (S n).
  2:{ admit. }
  (* La regla de evaluación reds_suc sustituye el valor v en Heval por S n
   * al realizar la inversión (y sustituciones). *)

  apply HI.
  unfold anotacion in Ht1.
  assert (Ht1_ := Ht1 _ Hc _ _ X H1 HP).
  replace (val_refl (preservation X H1)) with n in Ht1_.
  exact Ht1_.
  admit.
  (* Esta prueba es similar a la prueba de la regla hoare_plus.
   * Aquí la subexpresión p es maneja de manera similar al caso del operando
   * t1 (o del operando t2) de la regla hoare_plus. *)
Admitted.
(* Notese que la forma de esta regla no sigue del todo la forma de la regla
 * hoare_succ en el caso del lenguaje funciona simple (NFuncional). La regla en
 * ese lenguaje es:
 *
 *   {{ P }} p {{ λn.Q (S n) }}
 *  ----------------------------  (hoare_<op>)
 *    {{ P }} S p {{ λn.Q n }}
 *
 * donde intuitiavmente se "levanta" la función sucesor de la expresión en el
 * lenguaje al nivel lógico.
 * En el lenguaje PCF no se puede hacer lo mismo ya que en una anotación
 * G |- {{ P }} t1 {{ Q }}, Q es una aserción que espera un valor v de algún tipo
 * T. Ese valor debe ser un número natural (y dentro de la prueba Coq
 * simplificará las expresiones necesarias y se dará cuenta que efectivamente el
 * tipo T es N valor del que habla Q es un númeroy v un número natural.
 * Por esto es necesario usar un guaratee "intermedio" Q' que habla del
 * resultado de evaluar la subexpresión t1 y se hace explícitamente la
 * implicación Q' n -> Q (S n).
 * Esta regla se parece más a las reglas haore_plus y hoare_prod que tienen
 * aserciones "intermedias" Q1 y Q2 que hablan de los resultados de los
 * operandos de la operación en cuestión. *)

Theorem hoare_pred : forall G t1 P (Q : guarantee) Q',
    G |- {{ P }} t1 {{ Q' }} ->
    (forall n, Q' N n -> Q N (pred n)) ->
    G |- {{ P }} trm_pred t1 {{ Q }}.
Admitted.
(* Demostración completamente análoga a la demostración del teorema hoare_suc. *)

Theorem hoare_iz : forall G t1 P (Q : guarantee) Q',
    G |- {{ P }} t1 {{ Q' }} ->
    (Q' N 0 -> Q B true ) ->
    (forall n, Q' N (S n) -> Q B false) ->
    G |- {{ P }} trm_iz t1 {{ Q }}.
Proof.
  intros G t1 P Q Q' Ht1 HItt HIff.
  intros E Hc T v Htyping Heval HP.
  inversion Htyping; subst.
  inversion Heval; subst.

  - (* p se evalúa a 0. *)
    replace (val_refl (preservation Htyping Heval)) with true.
    2:{ admit. }
    (* La regla de evaluación reds_suc sustituye el valor v en Heval por true
     * al realizar la inversión (y sustituciones). *)

    apply HItt.
    unfold anotacion in Ht1.
    assert (Ht1_ := Ht1 _ Hc _ _ X H1 HP).
    replace (val_refl (preservation X H1)) with 0 in Ht1_.
    exact Ht1_.
    admit.
  - (* p se evalúa a S n. *)
    replace (val_refl (preservation Htyping Heval)) with false.
    2:{ admit. }
    (* La regla de evaluación reds_suc sustituye el valor v en Heval por false
     * al realizar la inversión (y sustituciones). *)

    apply HIff with n.
    unfold anotacion in Ht1.
    assert (Ht1_ := Ht1 _ Hc _ _ X H1 HP).
    replace (val_refl (preservation X H1)) with (S n) in Ht1_.
    exact Ht1_.
    admit.
Admitted.  

Theorem hoare_consequence : forall G t (P : rely) (Q : guarantee) P' Q',
    G |- {{ P' }} t {{ Q' }} ->
    (forall E, P E -> P' E) ->
    (forall T v, Q' T v -> Q T v) ->
    G |- {{ P }} t {{ Q }}.
Proof.
  intros G t P Q P' Q' Ht HIP HIQ.
  intros E Hc T v Htyping Heval HP.
  apply HIQ.
  apply Ht.
  assumption.
  apply HIP.
  assumption.
Qed.

Close Scope pcf_hoare_scope.

(* Fin de PCF_Hoare.v *)
