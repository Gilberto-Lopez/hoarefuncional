(* Reflexión lógica de los valores del lenguajde PCF en la lógica de Coq.
 * Dado un valor de PCF (un término sintáctico) de tipo T bajo el contexto de
 * tipos G y una prueba de tipado, la función val_refl regresa un habitante del
 * tipo [T], que es la reflexión lógica del tipo T en el sistema de tipos de
 * Coq. Este habitante de [T] representará el valor v en el nivel lógico, esto
 * es, las aserciones en las anotaciones de programas. *)

Require Import PCF_Types PCF_Lang PCF_Typing.

Set Implicit Arguments.

Definition val_refl (v : val) (T : typ) (G : ctx) : G |= v ~: T -> [T].
Proof.
  intros X;
  destruct v;
  inversion X;
  inversion X0;
  subst; simpl.
  - apply n.
  - apply true.
  - apply false.
  - apply (P,P0).
  - apply (P,P0).
Defined.

(* Fin de PCF_ValRefl.v *)
