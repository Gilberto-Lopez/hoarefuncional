(* Definición del sistema de tipado para el lenguaje PCF. *)

From TLC Require Import LibLN.
Require Import PCF_Types PCF_Lang.

(* Contextos de tipado. *)
Definition ctx := env typ.

(* Relación de tipado. *)
Reserved Notation "G |= t ~: T" (at level 69).

Inductive typing : ctx -> trm -> typ -> Type :=
| typing_var : forall G x T,
    ok G ->
    binds x T G ->
    G |= trm_fvar x ~: T
| typing_v : forall G v T,
    typing_val G v T ->
    G |= v ~: T
| typing_app : forall U T G t1 t2,
    G |= t1 ~: U ==> T ->
    G |= t2 ~: U ->
    G |= trm_app t1 t2 ~: T
| typing_plus : forall G t1 t2,
    G |= t1 ~: N ->
    G |= t2 ~: N ->
    G |= trm_plus t1 t2 ~: N
| typing_prod : forall G t1 t2,
    G |= t1 ~: N ->
    G |= t2 ~: N ->
    G |= trm_prod t1 t2 ~: N
| typing_suc : forall G t1,
    G |= t1 ~: N ->
    G |= trm_suc t1 ~: N
| typing_pred : forall G t1,
    G |= t1 ~: N ->
    G |= trm_pred t1 ~: N
| typing_iz : forall G t1,
    G |= t1 ~: N ->
    G |= trm_iz t1 ~: B
| typing_if : forall T G t1 t2 t3,
    G |= t1 ~: B ->
    G |= t2 ~: T ->
    G |= t3 ~: T ->
    G |= trm_if t1 t2 t3 ~: T
with typing_val : ctx -> val -> typ -> Type :=
     | typing_num : forall G n,
         typing_val G (trm_num n) N
     | typing_tt : forall G,
         typing_val G tt B
     | typing_ff : forall G,
         typing_val G ff B
     | typing_abs : forall L G U T P Q t1,
         (forall x, x \notin L -> G & x ~ U |= t1 ^^ trm_fvar x ~: T) ->
         typing_val G (trm_abs U T P Q t1) (U ==> T)
     | typing_fix : forall L G U T P Q t1,
         (forall f, f \notin L ->
          forall x, x \notin L \u \{f} ->
                    G & f ~ U ==> T & x ~ U |= open_t t1 (trm_fvar f) (trm_fvar x) ~: T) ->
         typing_val G (trm_fix U T P Q t1) (U ==> T)

where "G |= t ~: T" := (typing G t T).

(* Fin de PCF_Typing.v *)
