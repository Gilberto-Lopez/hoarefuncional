(* Relación de evaluación. Semántica operacional de paso grande.
 * El resultado de la evaluación es un valor, que debe ser un término.
 * En la evaluación de un fix, el fix se "desenvuelve".
 * No hay relación de evaluación para valores pues ya no hay cómputos por
 * realizar.
 * Usando un ambiente de evaluación, las variables libres se reducen a un valor
 * asignado por dicho ambiente.
 * Las variables ligadas desaparecen cuando se abren con una expresión o se
 * mantienen apuntando a una abstracción/fix pues los valores resultantes son
 * términos del lenguaje. *)

From TLC Require Import LibLN.
Require Import PCF_Lang.

(* Ambientes de evaluación con las definiciones de variables libres. *)
Definition eval_env := env val.

Reserved Notation "E ||- t ⇓ v" (at level 68).

Inductive reds : eval_env -> trm -> val -> Prop :=
(* Variables libres. *)
| reds_var : forall E x v,
    ok E ->
    binds x v E ->
    term_val v ->
    E ||- trm_fvar x ⇓ v
(* Valores. *)
| reds_val : forall E v,
    term_val v ->
    E ||- trm_val v ⇓ v
(* Apliación de una abstracción. *)
| reds_red : forall E U T P Q t3 v2 v3 t1 t2,
    E ||- t1 ⇓ (trm_abs U T P Q t3) ->
    E ||- t2 ⇓ v2 -> 
    E ||- t3 ^^ v2 ⇓ v3 ->
    E ||- trm_app t1 t2 ⇓ v3
(* Apliación de una función recursiva. *)
| reds_fix : forall E U T P Q t3 v2 v3 t1 t2,
    let fix_ := trm_fix U T P Q t3 in
    E ||- t1 ⇓ fix_ ->
    E ||- t2 ⇓ v2 ->
    E ||- open_t t3 fix_ v2 ⇓ v3 ->
    E ||- trm_app t1 t2 ⇓ v3
(* Suma. *)
| reds_plus : forall E n m t1 t2,
    E ||- t1 ⇓ trm_num n ->
    E ||- t2 ⇓ trm_num m ->
    E ||- trm_plus t1 t2 ⇓ trm_num (n + m)
(* Producto. *)
| reds_prod : forall E n m t1 t2,
    E ||- t1 ⇓ trm_num n ->
    E ||- t2 ⇓ trm_num m ->
    E ||- trm_prod t1 t2 ⇓ trm_num (n * m)
(* Sucesor. *)
| reds_suc : forall E n t1,
    E ||- t1 ⇓ trm_num n ->
    E ||- trm_suc t1 ⇓ trm_num (S n)
(* Predecesor. *)
| reds_pred : forall E n t1,
    E ||- t1 ⇓ trm_num n ->
    E ||- trm_pred t1 ⇓ trm_num (pred n)
(* IsZero, caso verdadero. *)
| reds_iz_tt : forall E t1,
    E ||- t1 ⇓ trm_num 0 ->
    E ||- trm_iz t1 ⇓ tt
(* IsZero, caso falso. *)
| reds_iz_ff : forall E n t1,
    E ||- t1 ⇓ trm_num (S n) ->
    E ||- trm_iz t1 ⇓ ff
(* If, guardia verdadera. *)
| reds_if_tt : forall E v g t1 t2,
    E ||- g ⇓ tt ->
    E ||- t1 ⇓ v ->
    E ||- trm_if g t1 t2 ⇓ v
(* If, guardia falsa. *)
| reds_if_ff : forall E v g t1 t2,
    E ||- g ⇓ ff ->
    E ||- t2 ⇓ v ->
    E ||- trm_if g t1 t2 ⇓ v

where "E ||- t ⇓ v" := (reds E t v).

(* Fin de PCF_Eval.v *)
